SHELL:=/bin/bash

# *_DIR variables refer to single level subdirectories of the top level topplot
# source directory. This simplifies the sed usage throughout this file.
BUILD_DIR:=build
DIST_DIR:=dist
ENV_DIR:=envdir
LOG_DIR:=logs
LOG_FILES:=$(wildcard $(LOG_DIR)/top.*.log)
PNG_FILES:=$(subst .,_,$(LOG_FILES))
PNG_FILES:=$(subst /,_,$(PNG_FILES))
PNG_FILES:=$(shell echo $(PNG_FILES) | sed -n 's/\([^ ]\+\) \?/$(DIST_DIR)\/\1_overview.png /gp')
TEST_DIR:=tests.d
TEST_EXE:=$(ENV_DIR)/bin/topplot
TOPPLOT_VERSION:=$(shell sed -n 's/^version = \([0-9.]\+\)$$/\1/p' setup.cfg)
TOPPLOT_WHEEL:=$(DIST_DIR)/topplot-$(TOPPLOT_VERSION)-py2.py3-none-any.whl


# If a specific point release isn't defined as the PYVER environment variable,
# default to whatever the default Python3 is
ifeq ($(PYVER),)
PYVER := 3
endif

#-------------------------------------------------------------------------------

.PHONY: build clean recreate.venv refresh.venv refresh_no_cache.venv test test-upload upload

.ONESHELL: # Allow multiline recipes. Note that early fails aren't detected.

#-------------------------------------------------------------------------------

clean:
	rm -rf $(BUILD_DIR) $(DIST_DIR) $(ENV_DIR) $(TEST_DIR) *.egg-info

#-------------------------------------------------------------------------------
# Remove and recreate the .venv dir for development work
#
# Note: this doesn't repopulate .venv. See below.

recreate.venv:
	rm -rf .venv || true
	python${PYVER} -m venv .venv

#-------------------------------------------------------------------------------
# (re)install the working .venv for development work
#
# This rule forcibly updates pip and setuptools, and skips the cache. This is
# intended to skirt any issues with out-of-date versions. This takes longer, so
# use refresh_.venv if you can.

refresh_no_cache.venv: recreate.venv
	source .venv/bin/activate
	pip3 install --no-cache-dir --ignore-installed -e .

#-------------------------------------------------------------------------------
# (re)install the working .venv for development work

refresh.venv: recreate.venv
	source .venv/bin/activate
	pip3 install -e .

#-------------------------------------------------------------------------------

$(TOPPLOT_WHEEL): setup.cfg setup.py pyproject.toml topplot/*.py topplot/*.png
	python3 -V
	python3 -m build --wheel --sdist

build: $(TOPPLOT_WHEEL)

#-------------------------------------------------------------------------------

$(TEST_EXE): $(TOPPLOT_WHEEL)
	[ -d $(ENV_DIR) ] && rm -rf $(ENV_DIR)
	python3 -m venv $(ENV_DIR)
	. $(ENV_DIR)/bin/activate
	pip3 install $(TOPPLOT_WHEEL)

install: $(TEST_EXE)

#-------------------------------------------------------------------------------

$(PNG_FILES): $(TEST_EXE) $(LOG_FILES)
	[ -d $(TEST_DIR) ] || mkdir -p $(TEST_DIR)
	. $(ENV_DIR)/bin/activate
	$(TEST_EXE) -f $(LOG_DIR)/$(shell echo $(@F) | sed -e 's/'$(LOG_DIR)'_top_\(.*\)_log_overview\.png/top.\1.log/') -g0pcCQ --outputdir $(TEST_DIR)

test: $(PNG_FILES)

#-------------------------------------------------------------------------------

test-upload: $(TOPPLOT_WHEEL)
	echo ebardie | twine upload --verbose --repository-url https://test.pypi.org/legacy/ $(DIST_DIR)/*
	sleep 3

upload: clean $(TOPPLOT_WHEEL) $(PNG_FILES)
	echo ebardie | twine upload --verbose  $(DIST_DIR)/*

force-upload: $(TOPPLOT_WHEEL)
	echo ebardie | twine upload --verbose  $(DIST_DIR)/*

#-------------------------------------------------------------------------------
